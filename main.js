const path = require('path');
const url = require('url');
const {app, BrowserWindow} = require('electron');
const {createStore, applyMiddleware} = require('redux');
const {
    forwardToRenderer,
    triggerAlias,
    replayActionMain,
} = require('electron-redux');

const reducers = require("./src/redux/reducers/rootReducer");

const store = createStore(reducers, applyMiddleware(triggerAlias, forwardToRenderer));

replayActionMain(store);


function createWindow() {
    const win = new BrowserWindow({
        width: 1024,
        height: 768,
        frame: false,
        fullscreen: false,
        webPreferences: {
            nodeIntegration: true,
            // worldSafeExecuteJavaScript:true,
            // contextIsolation:true,
            // preload:path.join(__dirname,'preload'),
            enableRemoteModule: true
        },
        x:0,
        y:0
    })
    win.webContents.openDevTools()
    win.loadFile('./src/firstWindow/firstIndex.html');


    const secondWin = new BrowserWindow({
        width: 1024,
        height: 768,
        backgroundColor: "white",
        frame: true,
        fullscreen: false,
        webPreferences: {
            nodeIntegration: true,
            // worldSafeExecuteJavaScript:true,
            // contextIsolation:true,
            // preload:path.join(__dirname,'preload'),
            enableRemoteModule: true
        },
        x:1200,
        y:300
    })
    secondWin.webContents.openDevTools()
    secondWin.loadFile('./src/secondWindow/secondIndex.html')
}

require('electron-reload')(__dirname, {
    electron: path.join(__dirname, 'node_modules', '.bin', 'electron')
})


app.whenReady().then(createWindow)
