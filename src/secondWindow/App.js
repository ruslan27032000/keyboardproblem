import React from 'react';
import "./App.css"
import InitialPage from "../components/initialPage/InitialPage";
import {HashRouter as Router, Route, Switch} from 'react-router-dom'
import AutoNumber from "../components/AutoNumber/AutoNumber";

export default function App() {
    return (
        <Router>
            <Switch>
                <Route exact path={"/"} render={() =><InitialPage/>}/>
                <Route exact path={"/autoNumber"} render={()=><AutoNumber/>}/>
            </Switch>
        </Router>
    )
}
