import React, {useEffect, useState} from 'react';
import styles from "./AutoNumber.module.css"
import {useDispatch, useSelector} from "react-redux";
import Header from "../Header/Header";
import {useHistory} from "react-router-dom";
import autoNumber from "../../redux/actions/autoNumberActions";
import Keyboard from "react-simple-keyboard";
import "../KeyboardComponent/KeyboardComponent.css"
import "react-simple-keyboard/build/css/index.css";

const layout = {
    "shift": [
        "1 2 3 4 5 6 7 8 9 0",
        "Q W E R T Y U I O P",
        "A S D F G H J K L",
        "Z X C V B N M {backspace}"
    ]
}

const display = {
    "{backspace}": "⌫",
}

export default function AutoNumber({using}) {
    const history = useHistory();
    const dispatch = useDispatch()
    const {currentPage} = useSelector(state => state.changePageReducer)
    const {focus, value, user, ref} = useSelector(state => state.autoNumberReducer);
    const [keyboard,setKeyboard] = useState({})


    useEffect(() => {
        history.push(currentPage)
    }, [currentPage])

    useEffect(() => {

    }, [focus])


    const changeFocus = (focus) => {
        dispatch(new autoNumber().changeFocusAction(focus))
        dispatch(new autoNumber().changeUsingAction(using))
    }

    const onChangeInput = e => {
        keyboard.setInput(e.target.value);
    }

    const onChange = input => {
        console.log(input)
        dispatch(new autoNumber().changeValueAction(input))
    }

    console.log(keyboard)

    return (
        <>
            <Header previousPage={"/"}/>
            <div className={styles.main}>
                <span>Введите гос.номер автомобиля</span>
                <div className={styles.form}>
                    {/*<span style={focus ?{marginTop:"5px",fontSize:"25px"}:null}>Номер машины</span>*/}
                    <input
                        onFocus={() => changeFocus(true)}
                        onBlur={() => changeFocus(false)}
                        value={value}
                        onChange={onChangeInput}
                    />
                </div>
                <Keyboard
                    keyboardRef={r =>
                        setKeyboard(r)
                    }
                    layoutName="shift"
                    onChange={onChange}
                    layout={layout}
                    display={display}
                    theme={"hg-theme-default myTheme1"}
                />
            </div>
        </>
    )
}
