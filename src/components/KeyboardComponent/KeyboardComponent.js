import React, {useEffect, useState} from 'react';
import Keyboard from "react-simple-keyboard";
import "react-simple-keyboard/build/css/index.css";
import {useDispatch, useSelector} from "react-redux";
import "./KeyboardComponent.css"
import autoNumber from "../../redux/actions/autoNumberActions";

const layout = {
    "shift":[
        "1 2 3 4 5 6 7 8 9 0",
        "Q W E R T Y U I O P",
        "A S D F G H J K L",
        "Z X C V B N M {backspace}"
    ]
}

const display = {
    "{backspace}": "⌫",
}
export default function KeyboardComponent({actionFunction}) {
    const dispatch = useDispatch()
    const {ref} = useSelector(state => state.autoNumberReducer);
    const [keyboard, setKeyboard] = useState({})


    useEffect(()=>{
        dispatch(new autoNumber().setKeyboardAction(keyboard))
    },[])

    const onChange = (value) => {
        dispatch(new autoNumber().changeValueAction(value))
    }

    console.log(ref)

    return (
        <div>
            <Keyboard
                keyboardRef={r => {
                    setKeyboard(r)
                }}
                layoutName="shift"
                onChange={onChange}
                layout = {layout}
                display={display}
                theme={"hg-theme-default myTheme1"}
            />
        </div>
    );
}
