import React from 'react'
import styles from './Header.module.css'
import Logo from "../../img/Logo.png"
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import changePageAction from "../../redux/actions/changePageAction";



export default function Header({previousPage}) {
    const dispatch = useDispatch()

    const previousPageClick=()=>{
        dispatch(changePageAction(previousPage))
    }

    return (
        <div className={styles.container}>
            <Link className={styles.button} to={previousPage} onClick={previousPageClick}>Назад</Link>
            <img src={Logo}/>
        </div>
    )
}
