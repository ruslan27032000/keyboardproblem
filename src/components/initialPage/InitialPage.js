import React, {useEffect} from 'react'
import BackgroundSlider from "react-background-slider";
import {useHistory} from "react-router-dom";
import styles from "./InitialPage.module.css";
import freedom from "../../img/freedom.jpg"
import jysan from "../../img/jysan.jpg"
import {useDispatch, useSelector} from "react-redux";
import changePageAction from "../../redux/actions/changePageAction";




export default function InitialPage() {
    const {currentPage} = useSelector(state=>state.changePageReducer)
    const history = useHistory();
    const dispatch = useDispatch()


    useEffect(()=>{
        history.push(currentPage)
    },[currentPage])

    const changePage=()=>{
        dispatch(changePageAction("/autoNumber"))
    }

    console.log(currentPage)
    return (
        <>
            <BackgroundSlider
                images={[freedom,jysan]}
                duration={10} transition={2}/>
                <a className={styles.button} onClick={changePage}>Купить</a>
        </>
    )
}
