const CHANGE_PAGE = require("../../types/changePageTypes");

const initialState = {
    currentPage: "/autoNumber"
}

const changePageReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_PAGE:
            return {
                ...state,
                currentPage: action.payload
            }
        default:
            return {
                ...state
            }
    }
}

module.exports = changePageReducer;
