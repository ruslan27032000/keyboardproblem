const {CHANGE_FOCUS,CHANGE_VALUE,CHANGE_USER} = require("../../types/autoNumberTypes");


const initialState = {
    focus: false,
    value: "",
}

const autoNumberReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_FOCUS:
            return {
                ...state,
                focus: action.payload,
            }
        case CHANGE_VALUE:
            return {
                ...state,
                value: action.payload
            }
        default:
            return {
                ...state
            }
    }
}


module.exports = autoNumberReducer;
