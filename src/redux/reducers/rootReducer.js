const {combineReducers} =  require("redux");
const changePageReducer = require("./changePage/changePageReducer");
const autoNumberReducer = require("./autoNumber/autoNumberReducer");


let reducers = combineReducers({
    changePageReducer,
    autoNumberReducer
})

module.exports = reducers;
