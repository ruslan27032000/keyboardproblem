const {applyMiddleware, createStore} = require('redux')
const {forwardToMain, getInitialStateRenderer} = require('electron-redux');
const reducers = require("./reducers/rootReducer");


const initialState = getInitialStateRenderer();

const store = createStore(reducers, initialState, applyMiddleware(forwardToMain));

module.exports = store



