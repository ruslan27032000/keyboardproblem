
const {CHANGE_FOCUS, CHANGE_VALUE,CHANGE_USER,SET_KEYBOARD} = require("../types/autoNumberTypes");


class autoNumber {
    changeFocusAction(focus) {
        return {
            type: CHANGE_FOCUS,
            payload: focus
        }
    }

    changeValueAction(value) {
        return {
            type: CHANGE_VALUE,
            payload: value
        }
    }

    changeUsingAction(user){
        return{
            type:CHANGE_USER,
            payload:user
        }
    }

}


module.exports = autoNumber;
