const CHANGE_PAGE = require("../types/changePageTypes");


const changePageAction = (page)=>{
    return{
        type:CHANGE_PAGE,
        payload:page
    }
}

module.exports = changePageAction;
